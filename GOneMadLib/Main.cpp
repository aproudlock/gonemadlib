
#include <iostream>
#include <conio.h>
#include <string> 
#include <fstream>

using namespace std;
string PrintStory(string  answers[12]);
int main()
{	
	string questions[12] = {"Enter an adjective (describing word): ",
	"Enter a sport: ",
	"Enter city: ", 
	"Enter a person: ",
	"Enter an action verb: ",
	"Enter a car: ",
	"Enter a place: ",
	"Enter a noun: (things, plural): ",
	"Enter an adjective: ",
	"Enter a food: ",
	"Enter a liquid: ",
	"Enter an adjective: (describing word): "};
	string answers[12] = {};
	for (int i = 0; i < 12; i++) {
		cout << questions[i];
		getline(cin, answers[i]);
	}
	
	string story = PrintStory(answers);
	cout << "\n" << story;
	char yesOrNO;
	cout << "Would you like to save output file? (y/n)";
	cin >> yesOrNO;
	if (yesOrNO == 'y' || yesOrNO == 'Y') {
	string filepath = "madlib.txt";
	ofstream ofs(filepath);
	ofs << story;
	cout << "Mad lib has been saved to " << filepath << "\n";
	}
	cout << "Press any key to quit \n";

	(void)_getch();
	return 0;
}
string PrintStory(string  answers[12])
{

	return  "One day my " + answers[0] + " friend and I decided to go to the " + answers[1] + " game in " + answers[2] + ". \n"
		+ "We really wanted to see " + answers[3] + " play.\n"
		+ "So we" + answers[4] + " in the " + answers[5] + " and headed down to the " + answers[6] + " and bought some " + answers[7] + ".\n"
		+ "We watched the game and it was " + answers[8] + ".\n"
		+ "We ate some " + answers[9] + " and drank " + answers[10] + ".\n"
		+ "We had a " + answers[11] + " time, and can't wait to go again. \n \n";
	
}
